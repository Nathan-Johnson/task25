﻿using System;

namespace task25
{
    //Task A
     class Exercise 
    {
        int Num1;
        int Num2;

        public Exercise(int _num1, int _num2)
        {
            Num1 = _num1;
            Num2 = _num2;
        }
        public void Calc()
        {
            Console.WriteLine($"{Num1}, {Num2}");
        }

    }
    //Task B
    class Checkout
    {
        int Quantity;
        double Price;
        public Checkout(int _quantity, double _price)
        {
            Quantity = _quantity;
            Price = _price;
        }
        public double Total()
        {
            return Quantity*Price;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //task A
            var number1 = new Exercise(2, 2);
            number1.Calc();
            Console.Clear();

            //task B
            var p1 = new Checkout(2, 4.50);
            p1.Total();
            var p2 = new Checkout(1, 10.95);
            p2.Total();
            var p3 = new Checkout(7, 0.99);
            p3.Total();
            
            Console.WriteLine($"{p1.Total()+p2.Total()+p3.Total():C2}");
        }
    }
}
